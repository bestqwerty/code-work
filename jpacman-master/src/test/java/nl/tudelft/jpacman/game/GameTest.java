package nl.tudelft.jpacman.game;

import nl.tudelft.jpacman.Launcher;
import nl.tudelft.jpacman.board.Direction;
import nl.tudelft.jpacman.level.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class GameTest {

    private Launcher launcher;

    @BeforeEach
    public void setUp() {
        launcher = new Launcher().withMapFile("/border.txt");
        launcher.launch();
    }

    @AfterEach
    public void endGame() {
        launcher.dispose();
    }

    @Test
    public void testStart() {
        Game game = launcher.getGame();
        assertThat(game.isInProgress()).isEqualTo(false);
        game.start();
        assertThat(game.isInProgress()).isEqualTo(true);
    }

    @Test
    public void testStartToStop() {
        Game game = launcher.getGame();
        game.start();
        assertThat(game.isInProgress()).isEqualTo(true);
        game.stop();
        assertThat(game.isInProgress()).isEqualTo(false);
    }

    @Test
    public void testStopToStart() {
        Game game = launcher.getGame();
        game.start();
        game.stop();
        assertThat(game.isInProgress()).isEqualTo(false);
        game.start();
        assertThat(game.isInProgress()).isEqualTo(true);
    }

    @Test
    public void testWin() {
        Game game = launcher.getGame();
        Player player = game.getPlayers().get(0);
        game.start();
        game.move(player, Direction.EAST);
        assertThat(game.isInProgress()).isEqualTo(false);
        assertThat(player.isAlive()).isEqualTo(true);
    }

    @Test
    public void testFail() throws InterruptedException {
        Game game = launcher.getGame();
        Player player = game.getPlayers().get(0);
        game.start();
        Thread.sleep(9000L);
        assertThat(game.isInProgress()).isEqualTo(false);
        assertThat(player.isAlive()).isEqualTo(false);
    }
}

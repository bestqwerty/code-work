package nl.tudelft.jpacman.npc.ghost;

import com.google.common.collect.Lists;
import nl.tudelft.jpacman.board.BoardFactory;
import nl.tudelft.jpacman.board.Direction;
import nl.tudelft.jpacman.level.*;
import nl.tudelft.jpacman.points.PointCalculator;
import nl.tudelft.jpacman.sprite.PacManSprites;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ClydeTest {
    private static MapParser mapParser;
    private static PlayerFactory playerFactory;
    @DisplayName("如果PLayer不存在，Clyde无法判断出发位置")
    @Test
    void when_Not_Exist_Player() {
        List<String> mapString = Lists.newArrayList(
            "##############",
            "#...#..C......",
            "##############"
        );
        Level level = mapParser.parseMap(mapString);
        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());
        assertThat(clyde.hasSquare()).isEqualTo(true);
        assertThat(clyde.getDirection()).isEqualTo(Direction.EAST);
        Optional<Direction> nextAiMove = clyde.nextAiMove();
        assertThat(nextAiMove).isEqualTo(Optional.empty());
    }
    @BeforeAll
    static void beforeAll() {
        PacManSprites pacManSprites = new PacManSprites();
        playerFactory = new PlayerFactory(pacManSprites);
        GhostFactory ghostFactory = new GhostFactory(pacManSprites);
        LevelFactory levelFactory = new LevelFactory(
            pacManSprites,
            ghostFactory,
            mock(PointCalculator.class));
        BoardFactory boardFactory = new BoardFactory(pacManSprites);
        GhostMapParser ghostMapParser = new GhostMapParser(
            levelFactory,
            boardFactory,
            ghostFactory);
        mapParser = ghostMapParser;
    }
    @DisplayName("Clyde无法到达到Player")
    @Test
    void when_Unable_To_Reach() {
        List<String> mapString = Lists.newArrayList(
            "##############",
            "#...C..#.....P",
            "##############"
        );
        Level level = mapParser.parseMap(mapString);
        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());
        assertThat(clyde.hasSquare()).isEqualTo(true);
        assertThat(clyde.getDirection()).isEqualTo(Direction.EAST);
        Player player = playerFactory.createPacMan();
        player.setDirection(Direction.valueOf("WEST"));
        level.registerPlayer(player);
        Player p = Navigation.findUnitInBoard(Player.class, level.getBoard());
        assertThat(p.hasSquare()).isEqualTo(true);
        assertThat(p.getDirection()).isEqualTo(Direction.WEST);
        Optional<Direction> nextAiMove = clyde.nextAiMove();
        assertThat(nextAiMove).isEqualTo(Optional.empty());
    }

    @DisplayName("Clyde离Player距离小于8")
    @Test
    void when_Distance_Less_SHYNESS() {
        List<String> mapString = Lists.newArrayList(
            "##############",
            "#.#....C.....P",
            "##############"
        );
        Level level = mapParser.parseMap(mapString);
        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());
        assertThat(clyde.hasSquare()).isEqualTo(true);
        assertThat(clyde.getDirection()).isEqualTo(Direction.EAST);
        Player player = playerFactory.createPacMan();
        player.setDirection(Direction.valueOf("WEST"));
        level.registerPlayer(player);
        Player p = Navigation.findUnitInBoard(Player.class, level.getBoard());
        assertThat(p.hasSquare()).isEqualTo(true);
        assertThat(p.getDirection()).isEqualTo(Direction.WEST);
        Optional<Direction> nextAiMove = clyde.nextAiMove();
        assertThat(nextAiMove.get()).isEqualTo(Direction.WEST);
    }

    @DisplayName("Clyde离Player距离大于8")
    @Test
    void when_Distance_Greater_SHYNESS() {
        List<String> mapString = Lists.newArrayList(
            "##############",
            "#...C........P",
            "##############"
        );
        Level level = mapParser.parseMap(mapString);
        Clyde clyde = Navigation.findUnitInBoard(Clyde.class, level.getBoard());
        assertThat(clyde.hasSquare()).isEqualTo(true);
        assertThat(clyde.getDirection()).isEqualTo(Direction.EAST);
        Player player = playerFactory.createPacMan();
        player.setDirection(Direction.valueOf("WEST"));
        level.registerPlayer(player);
        Player p = Navigation.findUnitInBoard(Player.class, level.getBoard());
        assertThat(p.hasSquare()).isEqualTo(true);
        assertThat(p.getDirection()).isEqualTo(Direction.WEST);
        Optional<Direction> nextAiMove = clyde.nextAiMove();
        assertThat(nextAiMove.get()).isEqualTo(Direction.EAST);
    }


}

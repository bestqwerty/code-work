package nl.tudelft.jpacman.npc.ghost;

import static org.junit.jupiter.api.Assertions.*;

import com.google.common.collect.Lists;
import nl.tudelft.jpacman.board.BoardFactory;
import nl.tudelft.jpacman.board.Direction;
import nl.tudelft.jpacman.level.*;
import nl.tudelft.jpacman.points.PointCalculator;
import nl.tudelft.jpacman.sprite.PacManSprites;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;


class InkyTest {
    private static MapParser mapParser;
    private static PlayerFactory playerFactory;
    @BeforeAll
    static void beforeAll() {
        PacManSprites pacManSprites = new PacManSprites();
        playerFactory = new PlayerFactory(pacManSprites);
        GhostFactory ghostFactory = new GhostFactory(pacManSprites);
        LevelFactory levelFactory = new LevelFactory(
            pacManSprites,
            ghostFactory,
            mock(PointCalculator.class));
        BoardFactory boardFactory = new BoardFactory(pacManSprites);
        GhostMapParser ghostMapParser = new GhostMapParser(
            levelFactory,
            boardFactory,
            ghostFactory);
        mapParser = ghostMapParser;
    }
    @DisplayName("当Blinky不存在时，Inky不知道出发位置")
    @Test
    void when_Not_Exist_Blinky() {
        List<String> mapString = Lists.newArrayList(
            "##############",
            "#...#..I.....P",
            "##############"
        );
        Level level = mapParser.parseMap(mapString);
        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());
        assertThat(inky.hasSquare()).isEqualTo(true);
        assertThat(inky.getDirection()).isEqualTo(Direction.EAST);
        Player player = playerFactory.createPacMan();
        player.setDirection(Direction.valueOf("WEST"));
        level.registerPlayer(player);
        Player p = Navigation.findUnitInBoard(Player.class, level.getBoard());
        assertThat(p.hasSquare()).isEqualTo(true);
        assertThat(p.getDirection()).isEqualTo(Direction.WEST);
        Optional<Direction> nextAiMove = inky.nextAiMove();
        assertThat(nextAiMove).isEqualTo(Optional.empty());
    }


    @DisplayName("当player不存在时，Inky不知道出发位置")
    @Test
    void when_Not_Exist_Player() {
        List<String> mapString = Lists.newArrayList(
            "##############",
            "#...#..I.....B",
            "##############"
        );
        Level level = mapParser.parseMap(mapString);
        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());
        Blinky blinky = Navigation.findUnitInBoard(Blinky.class, level.getBoard());
        assertThat(inky.hasSquare()).isEqualTo(true);
        assertThat(blinky.hasSquare()).isEqualTo(true);
        assertThat(inky.getDirection()).isEqualTo(Direction.EAST);
        assertThat(blinky.getDirection()).isEqualTo(Direction.EAST);
        Optional<Direction> nextAiMove = inky.nextAiMove();
        assertThat(nextAiMove).isEqualTo(Optional.empty());
    }
    @DisplayName("当B的位置刚好处在playerDestination位置时，Inky应该无法判断从何处出发")
    @Test
    void when_At_The_Location_of_playerDestination() {
        List<String> mapString = org.assertj.core.util.Lists.newArrayList(
            "##############",
            "#I........B.P.",
            "##############"
        );

        Level level = mapParser.parseMap(mapString);

        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());
        Blinky blinky = Navigation.findUnitInBoard(Blinky.class, level.getBoard());
        assertThat(inky.hasSquare()).isEqualTo(true);
        assertThat(blinky.hasSquare()).isEqualTo(true);
        assertThat(inky.getDirection()).isEqualTo(Direction.EAST);
        assertThat(blinky.getDirection()).isEqualTo(Direction.EAST);
        Player player = playerFactory.createPacMan();
        player.setDirection(Direction.valueOf("WEST"));
        level.registerPlayer(player);
        Player p = Navigation.findUnitInBoard(Player.class, level.getBoard());
        assertThat(p.hasSquare()).isEqualTo(true);
        assertThat(p.getDirection()).isEqualTo(Direction.WEST);
        Optional<Direction> nextAiMove = inky.nextAiMove();
        assertThat(nextAiMove).isEqualTo(Optional.empty());
    }

    @DisplayName("当存在Inky到destination的路径时，Inky应该向destination方向移动")
    @Test
    void when_Exist_path_from_inky_to_destination() {
        List<String> mapString = Lists.newArrayList(
            "##############",
            "#.I..B......P.",
            "##############"
        );
        Level level = mapParser.parseMap(mapString);
        Inky inky = Navigation.findUnitInBoard(Inky.class, level.getBoard());
        Blinky blinky = Navigation.findUnitInBoard(Blinky.class, level.getBoard());
        assertThat(inky.hasSquare()).isEqualTo(true);
        assertThat(blinky.hasSquare()).isEqualTo(true);
        assertThat(inky.getDirection()).isEqualTo(Direction.EAST);
        assertThat(blinky.getDirection()).isEqualTo(Direction.EAST);
        Player player = playerFactory.createPacMan();
        player.setDirection(Direction.valueOf("WEST"));
        level.registerPlayer(player);
        Player p = Navigation.findUnitInBoard(Player.class, level.getBoard());
        assertThat(p.hasSquare()).isEqualTo(true);
        assertThat(p.getDirection()).isEqualTo(Direction.WEST);
        Optional<Direction> nextAiMove = inky.nextAiMove();
        assertThat(nextAiMove.get()).isEqualTo(Direction.WEST);
    }
}

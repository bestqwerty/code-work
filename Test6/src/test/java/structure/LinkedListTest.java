package structure;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.LinkedList;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class LinkedListTest {


    @Test
    @Order(1)
    @DisplayName("first,1,2,11,last")
    void testOne() {
        LinkedList<String> list = new LinkedList<>();
        assertThat(list.remove(" ")).isEqualTo(false);
    }

    @Test
    @Order(2)
    @DisplayName("first,6,7,11,last")
    void testTwo() {
        LinkedList<String> list = new LinkedList<>();
        list.add("12");
        assertThat(list.remove(" ")).isEqualTo(false);
    }

    @Test
    @Order(3)
    @DisplayName("first,1,2,3,4,5,last")
    void test3() {
        LinkedList<String> list = new LinkedList<>();
        list.add(" ");
        list.add("5");
        assertThat(list.remove(" ")).isEqualTo(true);
    }

    @Test
    @Order(4)
    @DisplayName("first,6,7,8,9,10,last")
    void test4() {
        LinkedList<String> list = new LinkedList<>();
        list.add(" ");
        assertThat(list.remove("5")).isEqualTo(false);
    }

    @Test
    @Order(5)
    @DisplayName("first,1,2,3,2,3,4,5,last")
    void test5() {
        LinkedList<String> list = new LinkedList<>();
        list.add("2");
        list.add("3");
        assertThat(list.remove("5")).isEqualTo(false);
    }

    @Test
    @Order(6)
    @DisplayName("first,6,7,8,7,8,9,10,last")
    void test6() {
        LinkedList<String> list = new LinkedList<>();
        list.add("12");
        list.add("13");
        list.add("14");
        assertThat(list.remove("13")).isEqualTo(true);
    }
}

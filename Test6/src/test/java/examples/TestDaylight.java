package examples;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.zone.ZoneRules;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestDaylight {

    @Test
    public void testDaylight() {
        ZonedDateTime now = ZonedDateTime.now( ZoneId.of( "America/Montreal" ) );
        ZoneId z = now.getZone();

        ZoneRules zoneRules = z.getRules();

        Boolean isDst = zoneRules.isDaylightSavings( now.toInstant() );
        
        log.info("处于夏令时"+isDst);
    }
    @Test
    public void testDaylightBeijing() {
        ZonedDateTime now = ZonedDateTime.now( ZoneId.of( "Asia/Shanghai" ) );
        ZoneId z = now.getZone();
        
        ZoneRules zoneRules = z.getRules();
        
        Boolean isDst = zoneRules.isDaylightSavings( now.toInstant() );
        
        log.info("处于夏令时"+isDst);
    }
}

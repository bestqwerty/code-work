package currency;

import java.math.BigDecimal;
import java.text.NumberFormat;

import org.junit.jupiter.api.Test;

public class TestCurrency {

    @Test
    void testCurrencyFormat() {
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        BigDecimal money = new BigDecimal(String.valueOf(100000));
        System.out.println(currency.format(money));
        
        CurrencyType a = CurrencyType.CAD;
        System.out.println(a.getName());
        
        ExchangeRateService exchangeRateService = new ExchangeRateServiceImpl();
        System.out.println(exchangeRateService.exchangeRate(CurrencyType.RMB, CurrencyType.USD));
    }
}

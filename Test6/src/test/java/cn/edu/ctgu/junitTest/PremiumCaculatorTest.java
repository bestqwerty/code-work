package cn.edu.ctgu.junitTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;

class PremiumCaculatorTest {

    @Test
    void testSexPointOfFemale() {
        PremiumCaculator premiumCaculator = PremiumCaculator.builder()
                .sex('F')
                .build();
        
        assertThat(premiumCaculator.getSexPoint()).isEqualTo(3);
    }
    
    @Test
    void testSexPointOfMale() {
        PremiumCaculator premiumCaculator = PremiumCaculator.builder()
                .sex('m')
                .build();
        
        assertThat(premiumCaculator.getSexPoint()).isEqualTo(4);
    }
    
    @Test
    void testSexPointOfErrorParser() {
        final PremiumCaculator premiumCaculator = PremiumCaculator.builder()
                .sex('g')
                .build();
        assertThatThrownBy(( ) -> premiumCaculator.getSexPoint())
        .isInstanceOf(PremiumCaculatorException.class)
        .hasMessage("性别参数不正确");
    }
    
}

package cn.edu.ctgu.junitTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


/***
 * 
 * @author tiger
 * @date 2021年3月4日-下午8:43:13
 * @description 三角形测试用例
 */
public class TriangleByParameterizeTest {
    private final Triangle triangle = new Triangle();
    
    @ParameterizedTest
    @CsvSource(
            { 
                "0,4,5,输入错误", 
                "3,4,6,不等边三角形", 
                "3,3,5,等腰三角形", 
                "3,3,3,等边三角形", 
                "3,3,6,非三角形" 
              })
    void paramTriangle(int a, int b, int c, String expected) {
        String type = triangle.classify(a, b, c);

        assertEquals(expected, type);
    }
}

package cn.edu.ctgu.junitTest;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class PremiumCaculator {
    private int age;
    private char sex;
    private String marriage;
    private int raise;

    /**
     * 计算年龄点数
     * 
     * @return
     */
    public int getAgePoint() {
        if (age >= 1 && age < 20) {
            return 2;
        } else if (age <= 39) {
            return 6;
        } else if (age <= 59) {
            return 4;
        } else if (age <= 99) {
            return 2;
        } else {
            throw new PremiumCaculatorException("年龄参数不正确");
        }
    }

    /**
     * 返回性别点数
     * 
     * @return
     */
    public int getSexPoint() {
        if (sex == 'M' || sex == 'm') {
            return 4;
        } else if (sex == 'F' || sex == 'f') {
            return 3;
        } else {
            throw new PremiumCaculatorException("性别参数不正确");
        }
    }

    /**
     * 
     * @return
     */
    public int getMarriagePoint() {
        if ("已婚".equals(marriage)) {
            return 3;
        } else if ("未婚".equals(marriage)) {
            return 5;
        } else {
            throw new PremiumCaculatorException("婚姻参数不正确");
        }
    }

    /**
     * 计算抚养人数点数
     * 
     * @return
     */
    public int getRaisePoint() {
        if (raise == 0) {
            return 0;
        } else if (raise >= 1 && raise <= 2) {
            return 1;
        } else if (raise <= 4) {
            return 2;
        } else {
            return 3;
        }
    }
}

package currency;

import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CurrencyMainUI {

    private JFrame frame;
    private JTextField amount;
    private JTextField destValue;
    private Country selectedCountry;
    private final ButtonGroup buttonGroup = new ButtonGroup();
    private JLabel flag;
    
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    CurrencyMainUI window = new CurrencyMainUI();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public CurrencyMainUI() {
        CountryManager.init();
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setTitle("货币转换器");
        frame.setBounds(100, 100, 487, 453);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblNewLabel = new JLabel("人民币金额");
        lblNewLabel.setBounds(43, 64, 88, 25);
        frame.getContentPane().add(lblNewLabel);

        amount = new JTextField();
        amount.setBounds(250, 63, 177, 23);
        frame.getContentPane().add(amount);
        amount.setColumns(10);

        JLabel label = new JLabel("等价于");
        label.setBounds(43, 120, 54, 15);
        frame.getContentPane().add(label);

        destValue = new JTextField();
        destValue.setEditable(false);
        destValue.setBounds(250, 114, 177, 21);
        frame.getContentPane().add(destValue);
        destValue.setColumns(10);

        JButton caculate = new JButton("计算");
        caculate.addActionListener(e -> {
            try {
                Parser parser = new Parser(selectedCountry, amount.getText());
                parser.parse();

                Converter converter = new Converter(new ExchangeRateServiceImpl());
                double result = converter.convertTo(CurrencyType.RMB, parser.toCurrencyType(), parser.amount());

                destValue.setText(String.valueOf(result));
            } catch (ParseException exception) {
                JOptionPane.showMessageDialog(frame, exception.getMessage(),"消息对话框",JOptionPane.WARNING_MESSAGE);
            }
        });

        caculate.setBounds(318, 189, 93, 23);
        frame.getContentPane().add(caculate);

        JButton clear = new JButton("清除");
        clear.addActionListener(e -> {

            destValue.setText("");
            amount.setText("");

        });
        clear.setBounds(318, 237, 93, 23);
        frame.getContentPane().add(clear);

        JButton exit = new JButton("退出");
        exit.addActionListener(e ->System.exit(0));
        
        exit.setBounds(318, 288, 93, 23);
        frame.getContentPane().add(exit);

        // 初始化国家单选钮组
        initCountryOptions();
        
        // 国旗标签
        flag = new JLabel("");
        flag.setBounds(138, 104, 102, 63);
        frame.getContentPane().add(flag);

    }

    /**
     * 初始化国家单选组
     * 
     */
    private void initCountryOptions() {
        java.util.List<Country> countryStream = CountryManager.getAll();
        log.info(countryStream.size()+"");
        int top = 189;
        int height = 36;
        for(Country country:countryStream) {
            log.info("添加"+country.getName().getName());
            JRadioButton usd = new JRadioButton(country.getName().getName());
            usd.addActionListener(e -> selectCountry(country));
          
            buttonGroup.add(usd);
            
            usd.setBounds(38, top, 121, 23);
            top += height;
            
            frame.getContentPane().add(usd);
        }
        log.info("生成国家单选框");
        
    }

    /**
     * 选择国家
     * 记录选择的国家，且显示国家国旗
     * 
     * @param country
     */
    private void selectCountry(Country country) {
        selectedCountry = country;
        flag.setIcon(country.getFlag());
    }
    
}

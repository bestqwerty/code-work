package currency;

public class Converter {

    private ExchangeRateService exchangeRateService;
    
    
    public Converter(ExchangeRateService exchangeRateService) {
        super();
        this.exchangeRateService = exchangeRateService;
    }


    /**
     * 转换amount数额的货币数量
     * 
     * @param from
     * @param to
     * @param amout
     * @return
     */
    double convertTo(CurrencyType from,CurrencyType to, double amout) {
        return amout*exchangeRateService.exchangeRate(from, to);
    }
}

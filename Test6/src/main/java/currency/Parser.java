package currency;

public class Parser {

    // 国家
    private Country country;
    // 数量
    private String _amout;
    // 
    private double amount;
    
    public Parser(Country country, String amout) {
        super();
        this.country = country;
        this._amout = amout;
    }

    
    public void parseCountry() {
        if (null == country) {
            throw new ParseException("国家选择为空");
        }
    }
    public void parseAmount() {
        if (null == _amout || _amout.isEmpty()) {
            throw new ParseException("金额解析错误");
        }
        amount = Double.valueOf(_amout);
    }
    
    public void parse() {
        parseCountry();
        parseAmount();
    }
    public CurrencyType toCurrencyType() {
        return country.getCurrency();
    }
    
    public double amount() {
        return amount;
    }
}

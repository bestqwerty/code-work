package currency;

/**
 * 外汇比率服务接口
 * 
 * @author tiger
 * @date 2021年3月31日-下午4:28:24
 */
public interface ExchangeRateService {

    /**
     * 返回货币之间的汇率
     * 
     * @param from
     * @param to
     * @return
     */
    double exchangeRate(CurrencyType from, CurrencyType to);
}

package currency;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import lombok.extern.slf4j.Slf4j;

/**
 * 国家信息管理类
 * 
 * @author tiger
 * @date 2021年4月10日-下午10:26:34
 */
@Slf4j
public class CountryManager {
    
    // 私有构造函数
    private CountryManager() {
        super();
    }

    // 国家名称
    private static CountryName[] countryNames= {
            CountryName.US,
            CountryName.CA,
            CountryName.JP,
            CountryName.EU
    };
    // 国旗文件名
    private static String[] countryFlags= {
            "src/main/resources/usa.jpg",  
            "src/main/resources/cad.jpg",  
            "src/main/resources/jpa.jpg",  
            "src/main/resources/eur.jpg"
    };
    // 货币
    private static CurrencyType[] currencyTypes= {
            CurrencyType.USD,
            CurrencyType.CAD,
            CurrencyType.JPY,
            CurrencyType.EUR
    };
    
    // 国家信息列表
    private static EnumMap<CountryName, Country> countrys = new EnumMap<>(CountryName.class);
    
    /**
     * 加载图标文件
     * 
     * @param flagfile
     * @return
     */
    public static Icon loadIcon(String flagfile) {
        BufferedImage image;
        Icon flag = null;
        try {
            image = ImageIO.read(new File(flagfile));
            flag = new ImageIcon(image);
        } catch (IOException e) {
            log.error(flagfile+" 加载出错！");
        }
        return flag;
    }
    /**
     * 初始化国家资源
     */
    public static void init() {
        for(int i=0; i < countryNames.length; ++i) {
            CountryName name = countryNames[i];
            CurrencyType currencyType = currencyTypes[i];
            Icon icon = loadIcon(countryFlags[i]);
            
            Country country = new Country(
                    name, 
                    icon, 
                    currencyType);
            countrys.put(name, country);
        }
        log.info("加载国家信息完成");
    }
    
    /**
     * 返回所有加载的国家
     * 
     * @return
     */
    public static List<Country> getAll(){
        return new ArrayList<>(countrys.values());
    }
}

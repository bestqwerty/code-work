package currency;
/**
 * 货币类型
 * 
 * @author tiger
 * @date 2021年4月10日-下午10:23:13
 */
public enum CurrencyType {
    RMB("RMB"),
    USD("USD"),
    JPY("JPY"),
    EUR("EUR"),
    CAD("CAD");
    
    private final String name;
    
    private CurrencyType(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
}

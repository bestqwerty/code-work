package currency;

public enum CountryName {
    CHINA("中国","China"),
    US("美国","Us"),
    JP("日本","Japan"),
    EU("欧盟","EU"),
    CA("加拿大","Canada");
    
    private final String name;
    private final String en;
    
    private CountryName(String name,String en) {
        this.name = name;
        this.en = en;
    }
    
    public String getName() {
        return this.name;
    }
    
    public String getEn() {
        return en;
    }
}

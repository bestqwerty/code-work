package currency;

import java.util.EnumMap;
import java.util.Map;

/**
 * 外汇比率服务实现
 * 
 * @author tiger
 * @date 2021年4月10日-下午10:23:35
 */
public class ExchangeRateServiceImpl implements ExchangeRateService {

    private Map<CurrencyType, Double> rates = new EnumMap<>(CurrencyType.class);
    
    // 初始化块
    {
        rates.put(CurrencyType.USD, 0.1525);
        rates.put(CurrencyType.CAD, 0.1922);
        rates.put(CurrencyType.JPY, 18.8838);
        rates.put(CurrencyType.EUR, 0.1300);
    };
    
    @Override
    public double exchangeRate(CurrencyType from, CurrencyType to) {
        if (rates.containsKey(to)) {
            return rates.get(to);
        }
        return 0;
    }

}

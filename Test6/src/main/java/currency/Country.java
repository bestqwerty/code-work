package currency;
/**
 * 国家类
 * 
 * 提供国家基本信息，包括国家名称，国旗和货币
 * 
 * @author tiger
 * @date 2021年4月10日-下午2:46:30
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@AllArgsConstructor
public class Country {
    // 国家名称
    private CountryName name;
    // 国旗
    private Icon flag;
    // 货币
    private CurrencyType currency;
    /**
     * 初始化加载国旗
     * 
     * @param flagfile
     */
    public void init(String flagfile) {
        BufferedImage image;
        try {
            image = ImageIO.read(new File(flagfile));
            flag = new ImageIcon(image);
        } catch (IOException e) {
            log.error("加载国旗出错"+name);
        }
    }
    
}

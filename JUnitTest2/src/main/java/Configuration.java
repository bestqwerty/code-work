import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 *
 * @author tiger
 * @date 2021年3月8日-下午12:39:18
 */

    public class Configuration {
        private Properties props = new Properties();

        /**
         * @Descreption 读取属性文件
         **/
        public void fromFile(File confFile) {
            try (FileInputStream in = new FileInputStream(confFile);) {
                props.load(new InputStreamReader(in, StandardCharsets.UTF_8));
            } catch (IOException e) {
                throw new ValueParseException("配置文件解析错误");
            }
        }

        /**
         * @Descreption 获取键值
         **/
        public String getProp(String key) {
            return props.getProperty(key);
        }

        /**
         * @Descreption 获取布尔类型键值
         **/
        public boolean getBoolean(String key) {
            String value = props.getProperty(key);
            if ("true".equalsIgnoreCase(value)) {
                return true;
            }
            if ("false".equalsIgnoreCase(value)) {
                return false;
            }
            throw new ValueParseException("键值不存在");
        }

        /**
         * @Descreption 返回指定布尔值
         **/
        public Boolean getBooleanWithDefault(String key, Boolean defaultValue) {
            String value = props.getProperty(key);
            if ("true".equalsIgnoreCase(value)) {
                return true;
            } else if ("false".equalsIgnoreCase(value)) {
                return false;
            } else return defaultValue;
        }

        /**
         * @Descreption 返回指定键的整数值
         **/
        public Integer getInt(String key, int defaultValue) {
            String value = props.getProperty(key);
            if (value != null) {
                return Integer.parseInt(value);
            } else {
                return defaultValue;
            }
        }

        /**
         * @Descreption 返回指定的日期值
         **/
        public Date getDate(String key, Date defaultValue) throws ParseException {
            String value = props.getProperty(key);
            if (value != null) {
                return Utils.string2Date(value);
            } else {
                return defaultValue;
            }
        }
    }

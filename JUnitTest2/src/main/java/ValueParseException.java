/**
 *
 * @author tiger
 * @date 2021年3月8日-下午12:38:13
 */
public class ValueParseException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public ValueParseException() {
    }

    public ValueParseException(String message) {
        super(message);
    }

    public ValueParseException(Throwable cause) {
        super(cause);
    }

    public ValueParseException(String message, Throwable cause) {
        super(message, cause);
    }
}
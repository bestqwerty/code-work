import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertAll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class ConfigurationTest {
    @TempDir
    static Path tempDir;

    Configuration getConf(List list) throws IOException {
        Path path = tempDir.resolve("app.conf");
        Files.write(path, list);
        Configuration configuration = new Configuration();
        configuration.fromFile(path.toFile());
        return configuration;
    }

    @Test
    @DisplayName("当键存在时应该返回键值")
    void value_should_exit_when_key_exist() throws IOException {
        List<String> list = Arrays.asList(
                "ea=false",
                "closeable=true",
                "date=2021-04-06",
                "ljj=21",
                "ljj=20",
                "study=学习"
        );
        Configuration configuration = getConf(list);
        // act
        String value = configuration.getProp("ea");
        String value2 = configuration.getProp("closeable");
        String value3 = configuration.getProp("date");
        String value4 = configuration.getProp("ljj");
        String value5 = configuration.getProp("stude");
        // assert
        assertEquals("false", value);
        assertEquals("true", value2);
        assertEquals("2021-04-06", value3);
        assertEquals("21", value4);
        assertEquals("学习", value5);
    }

    @Test
    @DisplayName("测试键值不存在抛出指定异常")
    void throw_ValueParseException_when_key_not_exist() throws IOException {
        List<String> list = Arrays.asList(
                "qw=false"
        );
        Configuration conf = getConf(list);
        // act
        // assertThrows,测试是否抛出指定异常
        Throwable throwable = assertThrows(ValueParseException.class, () ->{
            conf.getBoolean("notexist");
        });
        Throwable throwable2 = assertThrows(ValueParseException.class, () ->{
            conf.getBoolean("bed");
        });
        // assert
        //assertALl 当所有实例通过才通过
        assertAll(
                () -> assertEquals("键值不存在",throwable.getMessage()),
                () -> assertEquals("键值不存在",throwable2.getMessage())
        );
    }

    @Test
    @DisplayName("当键值不存在返回指定布尔值")
    void return_bool_defaultValue_when_key_not_exist(){
        List<String> list = Arrays.asList(
                "qw=false",
                "as=true"
        );
        try {
            Configuration conf = getConf(list);
            Boolean value1 = conf.getBooleanWithDefault("qw", true);
            Boolean value2 = conf.getBooleanWithDefault("as", false);
            Boolean value3 = conf.getBooleanWithDefault("noexist", false);
            assertAll(
                    "测试键值不存在是否会返回默认布尔值",
                    () -> assertEquals(false, value1),
                    () -> assertEquals(true, value2),
                    () -> assertEquals(false, value3)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("当键值不存在返回指定整数值")
    void return_int_defaultValue_when_not_exist(){
        List<String> list = Arrays.asList(
                "qw=21",
                "as=234"
        );
        try {
            Configuration conf = getConf(list);
            Integer value1 = conf.getInt("qw", 100);
            Integer value2 = conf.getInt("as", 110);
            Integer value3 = conf.getInt("noexist", 200);
            assertAll(
                    "测试键值不存在是否会返回默认整数值",
                    () -> assertEquals(21,value1),
                    () -> assertEquals(234, value2),
                    () -> assertEquals(200, value3)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("当键值不存在返回指定日期值")
    void return_date_defaultValue_when_not_exist() throws ParseException {
        List<String> list = Arrays.asList(
                "ea=2020-02-13",
                "mm=2021-01-24"
        );
        try {
            Configuration conf = getConf(list);
            Date value1 = conf.getDate("qw", Utils.string2Date("2020-04-05"));
            Date value2 = conf.getDate("as", Utils.string2Date("2020-03-06"));
            Date value3 = conf.getDate("noexist", Utils.string2Date("2021-03-15"));
            assertAll(
                    "测试键值不存在是否会返回默认日期值",
                    () -> assertEquals(Utils.string2Date("2020-02-13"), value1),
                    () -> assertEquals(Utils.string2Date("2021-01-24"), value2),
                    () -> assertEquals(Utils.string2Date("2021-03-15"), value3)
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

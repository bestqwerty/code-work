import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

public class NextDateTest {
    @ParameterizedTest
    @CsvFileSource(resources = "NextDate函数测试用例.csv", numLinesToSkip = 1, encoding = "UTF-8")
    void test(int mon, int day, int year, String except){
        NextDate nextDate = new NextDate();
        String k = nextDate.nextDate(mon, day, year);
        assertEquals(except, k);
    }
}
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommissionTest {
    @ParameterizedTest
    @CsvSource({
            "1,2,32,90.5",
            "10,5,5,251",
            "2,24,32,191.5",
            "20,43,21,403",
            "222222,2222,2222,222"
    })
    void Test(int lock, int stock, int barrel, double except){
        Commission commission = new Commission();
        double realRes = commission.getCommission(lock, stock, barrel);
        assertEquals(except, realRes);
    }
}
public class Commission {
    boolean Check(int lock, int stock, int barrel){
        if(barrel>90||barrel<1||lock<1||lock>70||stock<1||stock>80)
            return false;
       else
           return true;
    }
    double getCommission(int lock, int stock, int barrel){
        double k = 0,lockCost = 45, stockCost = 30,barrelCost = 25;
        if(!Check(lock, stock, barrel)){
            return -1;
        }else {
            double sell = lock*lockCost + stock*stockCost + barrel*barrelCost;
            if(sell>1800){
                k=(sell-1800)*0.2 + (1800-1000)*0.15 + (1000)*0.1;
            }else if(sell>1000){
               k=(sell-1000)*0.15 + 1000*0.1;
            }else{
                k=sell*0.1;
            }
        }
        return k;
    }
}

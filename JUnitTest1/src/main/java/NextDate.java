public class NextDate {

    String nextDate(int mon, int day,   int year){
        if(!Check( mon,day, year))
            return "输入日期错误";
        int k;
        boolean isLeap = LeapYear(year);
        if(isLeap){
            k = (mon!=2?GetDay(mon):GetDay(mon)+1);
            if(day==k&&mon==12){
                day=(day+1)%k;
                mon=(mon+1)%12;
                year++;
            }else if(day==k&&mon<12){
                day=(day+1)%k;
                mon=(mon+1)%12;
            }else if(day != k){
                day++;
            }
        }else{
            k = GetDay(mon);
            if(day==k&&mon==12){
                day=(day+1)%k;
                mon=(mon+1)%12;
                year++;
            }else if(day==k&&mon<12){
                day=(day+1)%k;
                mon=(mon+1)%12;
            }else if(day != k){
                day++;
            }
        }
        return year+"年"+mon+"月"+day+"号";
    }

    boolean Check( int mon, int day, int year){
        if(year<1900||year>2050||mon>12||mon<1||day<1||day>32)
            return false;
        else{
            if(LeapYear(year)){
                if(mon==2&&day>(GetDay(mon)+1) ) return false;
                if(mon!=2&&day>GetDay(mon)) return false;
            }else{
                if(day>GetDay(mon)) return false;
            }
        }
        return true;
    }

    int GetDay(int mon){
        int k = 0;
        if(mon == 1 || mon == 3 || mon == 5 || mon == 7 || mon == 8 || mon == 10 || mon == 12){
            k = 31;
        }else if(mon == 2) {
            k = 28;
        }else {
            k = 30;
        }
        return k;
    }

    boolean LeapYear(int year){
        if((year%4 ==0&&year%100!=0)||(year%400==0)){
            return true;
        }else{
            return false;
        }
    }
}
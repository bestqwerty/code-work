package com.ctgu;

public class TwoIntegers2 {

    public int sum(int n, int m) {
        // 前置断言
        assert n >= 1 && n <= 99;
        assert m >= 1 && m <= 99;
        assert n + m <= 99 && n + m >= 0;
        return n + m;
    }

    public int sub(int n, int m) {
        // 前置断言
        assert n >= 1 && n <= 99;
        assert m >= 1 && m <= 99;
        assert m - n <= 99&& m - n >= 0;
        return m - n;
    }
}

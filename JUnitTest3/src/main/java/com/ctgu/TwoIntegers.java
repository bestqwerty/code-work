package com.ctgu;

public class TwoIntegers {

    public int sum(int n, int m) {
        // 前置断言
        assert n >= 1 && n <= 99;
        assert m >= 1 && m <= 99;
        assert n + m < 100;
        return n + m;
    }
}
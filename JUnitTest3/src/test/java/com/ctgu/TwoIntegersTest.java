package com.ctgu;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;


import java.util.stream.Stream;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class TwoIntegersTest {

    private final TwoIntegers adder = new TwoIntegers();

    // 无效输入测试用例
    @ParameterizedTest(name = "num1={0}, num2={1}")
    @CsvSource({
            "0, 50",
            "100, 50",
            "50, 0",
            "50, 100",
            "50, 50",
            "99, 50",
            "50, 98",
            "98, 50",
            "50, 99"
    })
    void sumInvalidCases(int num1, int num2) {
        Assertions.assertThrows(
                AssertionError.class,
                () -> adder.sum(num1, num2));
    }

    //有效输入测试用例
    @ParameterizedTest(name = "num1={0}, num2={1}, result={2}")
    @CsvSource({
            "50, 48, 98",
            "1, 50, 51",
            "2,50,52",
            "50, 49, 99",
            "50, 1, 51",
            "50, 2, 52",
    })
    void sumValidCases(int num1, int num2, int result) {
        Assertions.assertEquals(result, adder.sum(num1, num2));
    }

    //采用参数生成方法实现有效输入测试用例
    @ParameterizedTest(name = "num1={0}, num2={1}, result={2}")
    @MethodSource("generator")
    void sumValidCasesWithMethodParam(int num1, int num2, int result) {
        Assertions.assertEquals(result, adder.sum(num1, num2));
    }

    // 参数生成方法必须为static
    private static Stream<Arguments> generator(){
        return Stream.of(
                Arguments.of(50,48,98),
                Arguments.of(1,50,51),
                Arguments.of(2,50,52),
                Arguments.of(50,49,99),
                Arguments.of(50,1,51),
                Arguments.of(50,2,52)
        );
    }
}
package com.ctgu;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class TwoIntegers2Test {
    private final TwoIntegers2 adder = new TwoIntegers2();
    // 加法无效输入测试用例
    @ParameterizedTest(name = "num1={0}, num2={1}")
    @CsvFileSource(resources = "/suminvaild.csv", numLinesToSkip = 1)
    void sumInvalidCases(int num1, int num2) {
        Assertions.assertThrows(
                AssertionError.class,
                () -> adder.sum(num1, num2));
    }

    // 加法有效输入测试用例
    @ParameterizedTest(name = "num1={0}, num2={1}, result={2}")
    @CsvFileSource(resources = "/sumvaild.csv", numLinesToSkip = 1)
    void sumValidCases(int num1, int num2, int result) {
        Assertions.assertEquals(result, adder.sum(num1, num2));
    }


    // 减法无效测试用例
    @ParameterizedTest(name = "num1 = {0}, num2 = {1}")
    @CsvFileSource(resources = "/subinvaild.csv", numLinesToSkip = 1)
    void subInvaildCase(int num1, int num2) {
        Assertions.assertThrows(
                AssertionError.class,
                () -> adder.sub(num1, num2)
        );
    }

    // 减法有效测试
    @ParameterizedTest(name = "num1 = {0}, num2 = {1}, result = {2}")
    @CsvFileSource(resources = "/subvaild.csv", numLinesToSkip = 1)
    void subVaildCase(int num1, int num2, int result) {
        Assertions.assertEquals(result, adder.sub(num1, num2));

    }
}

package Test07;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @ClassName PreminumCaculatorTest
 * @Description 测试类
 * @Version 1.0
 */
class PreminumCaculatorTest {

    private final PreminumCaculator preminumCaculator = new PreminumCaculator();

    @DisplayName("无效等价类测试")
    @ParameterizedTest
    @CsvFileSource(resources = "/InvalidTest.csv", numLinesToSkip = 1)
    void InvalidTest(int age, String gender, String marry, int adoptNum){
        Throwable throwable = assertThrows(ValueParamException.class,
                () -> preminumCaculator.getCalculate(age, gender, marry, adoptNum));
        System.out.print("age="+age+" ");
        System.out.print("gender="+gender+" ");
        System.out.print("marry="+marry+" ");
        System.out.print("adoptNum="+adoptNum+" ");
        System.out.println("res="+throwable.getMessage());
        assertEquals("输入错误", throwable.getMessage());
    }

    @DisplayName("有效等价类测试")
    @ParameterizedTest
    @CsvFileSource(resources = "/ValidTest.csv", numLinesToSkip = 1)
    void ValidTest(int age, String gender, String marry, int adoptNum, String insurence){
        String output = preminumCaculator.getCalculate(age,gender,marry,adoptNum);
        System.out.print("age="+age+" ");
        System.out.print("gender="+gender+" ");
        System.out.print("marry="+marry+" ");
        System.out.print("adoptNum="+adoptNum+" ");
        System.out.println("insurence="+insurence);
        assertEquals(insurence,output);
    }
}
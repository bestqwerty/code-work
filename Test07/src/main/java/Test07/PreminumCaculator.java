package Test07;

/**
 * @ClassName PreminumCaculator
 * @Description 保险公司人寿保险保费计算程序
 * @Version 1.0
 */
public class PreminumCaculator {

    int getAgePoint(int age){
        if(age >= 1 && age < 20) return  2;
        else if(age < 40) return  6;
        else if(age < 60) return  4;
        else  return  2;
    }

    int getGenderPoint(String gender) {
        int res = 0;
        if(gender.equals("男")) res = 4;
        else if(gender.equals("女")) res = 3;
        return res;
    }

    int getMarryPoint(String marry) {
        int res = 0;
        if(marry.equals("已婚")) res = 3;
        else if(marry.equals("未婚")) res = 5;
        return res;
    }

    double getAdoptPoint(int adoptNum) {
        double res = adoptNum*0.5;
        res = res > 3 ? 3 : res;
        return res;
    }

    String getCalculate(int age, String gender, String marry, int adoptNum) {
        if(age < 1 || age > 99) throw  new ValueParamException("输入错误");
        else if(adoptNum < 1) throw new ValueParamException("输入错误");
        int agePoint = getAgePoint(age);
        int genderPoint = getGenderPoint(gender);
        int marryPoint = getMarryPoint(marry);
        double adoptPoint = getAdoptPoint(adoptNum);
        double res = agePoint + genderPoint + marryPoint - adoptPoint;
        String insurance = res >= 10 ? "0.6%" : "0.1%";
        return insurance;
    }
}




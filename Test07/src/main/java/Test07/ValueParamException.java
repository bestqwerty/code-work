package Test07;

/**
 * @ClassName ValueParamException
 * @Description TODO
 * @Data 2021/4/7 11:36
 * @Version 1.0
 */
public class ValueParamException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    public ValueParamException(String message) {
        super(message);
    }
}

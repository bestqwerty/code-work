package com.ctgu;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumCaculatorTest {


    @ParameterizedTest
    @CsvFileSource(resources = "/等价类测试用例.csv", numLinesToSkip = 1, encoding = "UTF-8")
    void test(int a, char b, String c, int d,int e) {
        final PremiumCaculator premiumCaculator = new PremiumCaculator( a,  b,  c,  d);
        System.out.println("测试用例"+a+","+b+","+c+","+d+","+e);
       double type = premiumCaculator.PremiumCaculator(a,b,c,d);
        Assertions.assertEquals(e, type);
    }

    @Test
    void testSexPointOfFemale() {
        PremiumCaculator premiumCaculator = PremiumCaculator.builder()
                .sex('F')
                .build();

        assertThat(premiumCaculator.getSexPoint()).isEqualTo(3);
    }

    @Test
    void testSexPointOfMale() {
        PremiumCaculator premiumCaculator = PremiumCaculator.builder()
                .sex('m')
                .build();

        assertThat(premiumCaculator.getSexPoint()).isEqualTo(4);
    }

    @Test
    void testSexPointOfErrorParser() {
        PremiumCaculator premiumCaculator = PremiumCaculator.builder()
                .sex('g')
                .build();
        assertThatThrownBy(( ) -> premiumCaculator.getSexPoint())
                .isInstanceOf(PremiumCaculatorException.class)
                .hasMessage("性别参数不正确");
    }

}

package com.ctgu;

public class PremiumCaculatorException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public PremiumCaculatorException(String message) {
        super(message);
    }

}
